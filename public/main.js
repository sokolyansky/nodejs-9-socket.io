(function(){
  const socket = io();
  let username = '';
  $('#name').focus(); // или атрибут autofocus установить в разметке на элементе


  function addUser(name){
    let $user = $('<div/>', {
      class: 'users__item',
      text: name
    });

    // $user.data('name', name) не срабатывает для $('.users__item[data-name="' + data.username + '"]')
    // http://stackoverflow.com/questions/4191386/jquery-how-to-find-an-element-based-on-a-data-attribute-value
    //$user.data('name', name);  // http://ruseller.com/jquery.php?id=78
    $user.attr('data-name', name);
    //console.dir($user[0]);

    $('.users').append($user);
  }

  function addMessage(data){
    var $item = $('<div/>', {
      class: 'chat-history__item'
    });

    var $user = $('<div/>', {
      class: 'chat-history__item-user',
      text: data.username + ' : '
    });

    var $text = $('<div/>', {
      class: 'chat-history__item-text',
      text: data.message
    });

    $item.append($user);
    $item.append($text);

    $('.chat-history').append($item);
  }

  $('#name').keyup(function(event){
    let name = $(this).val(); // console.log(name);
    const enter =  $('.enter');

    if (name && event.which === 13)  // 13 - CR(Carriage return)
    {
      socket.emit('add user', name);
      socket.emit('login', name);
      $(this).val('');        // (redundant, for instance, since this element remove next line)
      enter.css('z-index', 0); // удаление css-св-ва (redundant, for instance, since this element remove next line)
      enter.prop('autofocus', false);
      enter.remove();
      $('#message').focus();
    }
  });

  $('#message').keyup(function(event){
    let message = $(this).val();

    if (message && event.which === 13)
    {
      socket.emit('new message', message);
      $(this).val('');

      addMessage({
        username: username,
        message: message
      });
    }
  });

  socket.on('login', function (data) {
    //$('.enter').hide();
    console.log('login');
    //username = data.username;

    addUser(data.username);
  });

  socket.on('new message', function (data) {
    addMessage(data);
  });

  socket.on('user joined', function (data) {
    addUser(data.username);
  });

  socket.on('user left', function (data) {
    //console.log($('.users__item').find('[data-name="' + data.username + '"]'));
    $('.users__item[data-name="' + data.username + '"]').remove();
  });
})();
