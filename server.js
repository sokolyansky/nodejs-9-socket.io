'use strict';

const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const port = process.env.PORT || 3000;

server.listen(port, () => {
  console.log('Server listening at port %d', port);
});

app.use(express.static(__dirname + '/public'));

// Chatroom

io.on('connection', socket => {

  let addedUser = false;

  socket.on('new message', data => {
    console.log('message', data);

    socket.broadcast.emit('new message', {
      username: socket.username,
      message: data
    });
  });

  socket.on('login', (data) => {
    socket.emit('login', {
      username: 'You entered as ' + data
    });
  });

  socket.on('add user', username => {
    // if (addedUser) return;

    socket.username = username;
    addedUser = true;

    console.log('login', username);

    socket.broadcast.emit('user joined', {
      username: socket.username
    });

  });

  socket.on('disconnect', function () {
    if (addedUser)
    {
      console.log(`user ${socket.username.toUpperCase()} left`);

      socket.broadcast.emit('user left', {
        username: socket.username
      });
    }
  });

});